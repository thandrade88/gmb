import $ from "jquery";
import Swiper from "swiper";
import jQueryBridget from 'jquery-bridget';
import isotope from 'isotope-layout';

import "bootstrap";
import "bootstrap/scss/bootstrap.scss";
import "@fortawesome/fontawesome-free/js/all.js";
import "@fortawesome/fontawesome-free/scss/fontawesome.scss";
import "swiper/dist/css/swiper.css";
import "./scss/main.scss";

  
  jQueryBridget( 'isotope', isotope, $ );
  let lastScrollTop = 0; 
  
  const includeHTML = () => {
    var z, i, elmnt, file, xhttp;
    /* Loop through a collection of all HTML elements: */
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
      elmnt = z[i];
      /*search for elements with a certain atrribute:*/
      file = elmnt.getAttribute("w3-include-html");
      if (file) {
        /* Make an HTTP request using the attribute value as the file name: */
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4) {
            if (this.status == 200) {elmnt.innerHTML = this.responseText;}
            if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
            /* Remove the attribute, and call this function once more: */
            elmnt.removeAttribute("w3-include-html");
            includeHTML();
          }
        }
        xhttp.open("GET", file, true);
        xhttp.send();
        /* Exit the function: */
        return;
      }
    }
  }
  includeHTML();
  
  const showPost = (id) => {
    let $wpURL= `https://gmbhospitalares.com.br/bloggmb/wp-json/wp/v2/posts/${id}?_embed`;
    
    $.ajax({
      type: 'GET',
      url: $wpURL,
      data: {},
      beforeSend: function(){
        $('#modal .modal-body').html();
        $('#modal .modal-body').html('<div class="loader"><i class="fas fa-spinner fa-pulse"></i></div>');
        $('#modal').modal('toggle');
      },
      success: function(post){        
        let html = () => `<article>
                            <div class='single-post'>
                              <h2>${post.title.rendered}</h2>
                              <img src="${post._embedded['wp:featuredmedia']['0'].source_url}" alt="${post._embedded['wp:featuredmedia']['0'].alt_text}">
                              <div class="content">
                                ${post.content.rendered}
                              </div>                 
                            </div>           
                          </article>`;           
                                        
        $('#modal .modal-dialog').addClass('modal-lg');
        $('#modal .modal-body').html(html);
        //$('#modal').modal('toggle');
        },
        error: function (request, textStatus, errorThrown) {
          $('#blog-posts').html('<div class="error">ERRO! Tivemos um erro ao carregar esta página. Por favor atualize a página para tentar novamente.</div>');
        }
    });   
    
    
    return id;
  }

  $(window).on('load',()=>{
    
    let page = window.location;
    const $navbar = $('header');

    $(window).on('scroll',(event) => {
      let st = window.scrollY;
      let gap = $( window ).height() - 115;

      if ($( document ).width() < 1040 ) gap = gap - 80;

      if ((st > lastScrollTop) && (st > gap)) { // scroll down

            $navbar.fadeOut();

      } else { // scroll up

          $navbar.fadeIn();

      }

      if(lastScrollTop > 400) $('.col-balls').removeClass('d-none'); else $('.col-balls').addClass('d-none');

      lastScrollTop = st;
    });

    let ClientsSwiper = new Swiper ('#slider-clients', {
      speed:1000,
      direction: 'horizontal',
      slidesPerView: 4,
      slidesPerGroup: 1,
      loop:true,
      spaceBetween: 0,
      autoplay: {
        delay: 3000,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        600: {
          slidesPerView: 1,
          spaceBetween: 5
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 10
        }
      },
    });

    let WebdoorSwiper = new Swiper ('#slider-webdoor', {
      speed:1000,
      direction: 'horizontal',
      slidesPerView: 1,
      slidesPerGroup: 1,
      loop:true,
      spaceBetween: 0,
      autoplay: {
        delay: 5000,
      },    
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },        
    });

    let $grid = $('.block-produtos').isotope({itemSelector: '.block',});
    
    $('.produtos-cat').on('change', (e)=> {
      let filterValue = e.currentTarget.value;        
      $grid.isotope({ filter: filterValue });
    });


    if((page.pathname.match(/\b(\w*produtos.html\w*)\b/g)) && (page.hash)) {
      $grid.isotope({filter: page.hash.replace('#','.')});
      $(".produtos-cat option[value='" + page.hash.replace('#','.') + "']").attr('selected',true);
    }
    
    if((page.pathname.match(/\b(\w*fale-conosco.html\w*)\b/g)) && (page.hash)) {
      $("#radioorcamento").prop('checked',true);
      $('.select-product').removeClass('d-none').attr('required',true);
      $('textarea.form-control').attr("placeholder", "* Acrescente mais algumas informações. Ex: quantidade, endereço, etc...");
    }

    if(page.pathname.match(/\b(\w*blog.html\w*)\b/g)) {

      let $wpURL="https://gmbhospitalares.com.br/bloggmb/wp-json/wp/v2/posts?";
      $wpURL = $wpURL + "_embed";
      $.ajax({
        type: 'GET',
        url: $wpURL,
        data: {},
        beforeSend: function(){
          $('#blog-posts').html('<div class="loader"><i class="fas fa-spinner fa-pulse"></i></div>');
        },
        success: function(data){
          let html = data.map((post)=>{
                          if(post._embedded['wp:featuredmedia']) {
                              return `
                                <article>
                                    <h2>${post.title.rendered}</h2>
                                    <img src="${post._embedded['wp:featuredmedia']['0'].source_url}" alt="${post._embedded['wp:featuredmedia']['0'].alt_text}">
                                    <div class="excerpt">
                                      ${post.excerpt.rendered}
                                    </div>
                                    <div class="link">
                                        <a href="#${post.id}" class="leia-mais">Leia Mais...</a>
                                    </div>
                                </article>
                              `;
                          }
                        });
            $('#blog-posts').html(html);
          },
          error: function (request, textStatus, errorThrown) {
            $('#blog-posts').html('<div class="error">ERRO! Tivemos um erro ao carregar esta página. Por favor atualize a página para tentar novamente.</div>');
          }
      });      
    }

    window.addEventListener("mousedown",function(event){
      if(event.target.className === 'leia-mais') {
        showPost(event.target.hash.replace('#',''));                
      }
     });  

    $("a.anchor-link").on('click', (event) => {
      event.preventDefault();           
        
        if (event.target.hash !== "") {
            
            let hash = event.target.hash;

            if (window.location.pathname !== '/') window.location = location.origin + '/' + hash;            
            
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000);
          
        }
    });
    
    $(".newsletter-form , .contact-form").submit(function(e){
        e.preventDefault();

        $.ajax({
            url: './sendform.php',
            type: 'post',
            data:{typeForm:$(this).data("form"),data:$(this).serialize()},
            success: function(data) {
              $('#modal .modal-body p').html(data);
              $('#modal').modal('toggle');

            }, error : function (erro) {
	            $('#modal .modal-body p').html('<div class="error">ERRO! Sua mensagem não foi enviada! Caso o erro persista, ligue para nós (21) 3269-3371 ou 999041-1689.</div>');
              $('#modal').modal('toggle');
            }
        });

    });

    $('.contact-form .form-check-input').on('change', () => {
      let formType = $('input[name=optradio]:checked', '.contact-form').val();
      
      if (formType == 'Orcamento') {
        $('.select-product').removeClass('d-none').attr('required',true);
        $('textarea.form-control').removeClass('contato');
        $('textarea.form-control').attr("placeholder", "* Acrescente mais algumas informações. Ex: quantidade, endereço, etc...");
      } else {
        $('.select-product').addClass('d-none').removeAttr('required');
        $('textarea.form-control').addClass('contato');
        $('textarea.form-control').attr("placeholder", "* Envie a sua mensagem");

      }
    });

    $('.ball-1').on('click', () => {	
      $('.ball').toggleClass('active');
    });   
    
    $('.block .btn').on('click', (e) => {      
      let parent = e.target.parentElement.parentElement.parentElement.classList.item(2),
          id = e.target.getAttribute('data-id'),
          content = $('.' + parent + ' .block-content-'+ id ).html();
    
      $('#modal-produto .modal-body .content').html('').html(content);
      $('#modal-produto').modal('toggle');
    });

    $('.btn-close').on('click', () => {
      $('#modal .modal-dialog').removeClass('modal-lg');
      $('.modal-produto').modal('hide');
    });    

    $('.navbar-toggler').on('click', () => {
      $('.body-page-produtos .navbar').toggleClass('bg-white');      
    });    

});