<?php
/**
 * Created by PhpStorm.
 * User: thales
 * Date: 01/12/2018
 * Time: 20:44
 */

header("allow-control-access-origin: * ");
parse_str($_REQUEST['data'], $data);

$to = 'atendimento@gmbhospitalares.com.br';
//$to = 'thandrade88@gmail.com';

$subject = $data['optradio'] ? $data['optradio'] : 'Newsletter ' . ' GMB';

$headers = "From: " . strip_tags($data['email']) . "\r\n";
$headers .= "Reply-To: ". strip_tags($data['email']) . "\r\n";
//$headers .= "CC: susan@example.com\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

if($_REQUEST['typeForm'] == 'contact') {
	$message = '<html><body>';
	$message .= '<h1>'.$data['name'].'</h1>';
	$message .= $data['produto'] ? '<p><strong>Produto:</strong> '.$data['produto'].'</p>' : "";
	$message .= '<p><strong>Telefone:</strong>	'.$data['phone'].'</p>';
	$message .= '<p><strong>Mensagem:</strong>	'.$data['message'].'</p>';
	$message .= '</body></html>';

	$thksMsg = '<div class="success">Agradecemos o seu interesse. Sua mensagem foi enviada com sucesso!</div>';
	if($data['optradio'] == 'Orcamento') $thksMsg = '<div class="success">Sua solicitação foi enviada com sucesso! Aguarde que retornaremos em até 48 horas.</div>';

} else {
	$message = '<html lang="en"><body>';
	$message .= '<p><strong>Novo email cadastrado:</strong>	'.$data['email'].'</p>';
	$message .= '</body></html>';
	$thksMsg = '<div class="success">Obrigado por assinar nossa Newsletter!</div>';
}

// send email
if(mail($to,$subject,$message,$headers)) { echo $thksMsg; }
else { echo '<div class="error">ERRO! Sua mensagem não foi enviada! Caso o erro persista, ligue para nós (21) 3269-3371 ou 999041-1689.</div>'; }
?>