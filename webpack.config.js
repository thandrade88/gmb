const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = {        
    entry: "./src/index.js",
    output: {
        path: __dirname + '/dist',
        filename: '[name].bundle.js'
    },    
    devServer: {
        historyApiFallback: true,
    },
    module: {
        rules: [
            {
                test: /\.js[x]?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [{
                    loader: "html-loader",
                    options: { minimize: false }
                }]
            },
            {
                test: /\.(png|jpe?g|gif)/i,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            name: "./img/[name].[ext]",
                            limit: 10000
                        }
                    },
                    {
                        loader: "img-loader"
                    }
                ]
            },
            {
                test: /\.(scss|css)/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    plugins: [        
        new HtmlWebPackPlugin({                     
            template: "./src/index.html",
            filename: "./index.html"
        }),
        new HtmlWebPackPlugin({            
            template: "./src/fale-conosco.html",
            filename: "./fale-conosco.html"
        }),      
        new HtmlWebPackPlugin({            
            template: "./src/produtos.html",
            filename: "./produtos.html"
        }),
        new HtmlWebPackPlugin({            
            template: "./src/blog.html",
            filename: "./blog.html"
        }),
        new HtmlWebPackPlugin({
            template: "./src/header.html",
            filename: "./header.html"
        }),
        new HtmlWebPackPlugin({
            template: "./src/footer.html",
            filename: "./footer.html"
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),            
    ],
    resolve: {
        extensions: ['*', '.js', '.jsx'],
        alias: {
            modules: __dirname + '/node_modules'
        }
    },
};